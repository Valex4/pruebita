create database FelicitArteDB;
use FelicitArteDB;

create table Users(
id int auto_increment primary key,
fullName varchar(100) not null,
gender varchar(40) not null,
birthDate date not null,
email varchar(40) not null,
password varchar(100) not null,
create_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
update_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

create table BirthdayBoy(
id int auto_increment primary key,
fullName varchar(100) not null,
birthDate DATE not null,
create_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
update_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
	
    
CREATE TABLE Cards(
  id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  name VARCHAR(50) NOT NULL,
  images LONGTEXT,
  background varchar(100),
  preview varchar(100),
  canvas_data TEXT NOT NULL,
  create_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  update_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
