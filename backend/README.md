# Pasos para ejecutar el proyecto

1. Debemos de crear un archivo .env en el cual debe de ir en el siguiente orden:


        DB_HOST= ""
        DB_USER= ""
        DB_DATABASE= FelicitArteDB
        DB_PASSWORD= ""
        SALT=10
        PORT_SERVER=4000
    

2. Para crear la base de datos tenemos una carpeta llamada Database, en el cual encontrará el archivo para crear la base de datos
    >FelicitArteDB.sql

3.Para ejecutar el proyecto debemos escribir en la consola:

    npm install
    npm run dev


----------------------------------------------------------------


