import express from "express";
import cors from "cors";
import { config } from "dotenv";
import "./connection.js"
import routerAuth from "./routes/authRoutes.js";
import routerBirthdayBoy from "./routes/birthdayBoyRoutes.js";
import routerFile from "./routes/fileRouter.js";
import routerCard from "./routes/cardRoute.js";
import path from "path";
import { fileURLToPath } from 'url';


const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const app= express();
config();
app.use(express.json());
app.use(cors());
app.use(express.static(path.join(__dirname, 'uploads')));
const port=process.env.PORT_SERVER;

app.listen(port,()=>{
    console.log("listening on port "+port);
});


app.use("/api",routerAuth)
app.use("/api",routerBirthdayBoy)
app.use("/api",routerFile)
app.use("/api",routerCard)