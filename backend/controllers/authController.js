import jwt from "jsonwebtoken";
import {
  createUser,
  getUserByEmail,
  comparePassword,
  resetPassword,
} from "./userController.js";
import {
  signUpBodyValidation,
  loginBodyValidation,
} from "../utils/validationSchema.js";
import { config } from "dotenv";

config();

export const signUp = async (req, res) => {
  //registra usuarios

  const { error } = signUpBodyValidation(req.body);
  if (error) {
    return res
      .status(400)
      .json({ error: error, message: error.details[0].message });
  }

  const { fullName, gender, birthDate, email, password } = req.body;
  await createUser(fullName, gender, birthDate, email, password);
  res.status(201).json({ message: "Acount created succesfuly!" });
};

export const signIn = async (req, res) => {
  //login

  const { error } = loginBodyValidation(req.body);
  if (error) {
    return res
      .status(400)
      .json({ error: error, message: error.details[0].message });
  }
  const { email, password } = req.body;

  const userFound = await getUserByEmail(email);
  console.log(userFound);

  if (!userFound) {
    return res.status(400).json({ message: " [Error] something goes wrong!" });
  }
  const matchpassword = await comparePassword(password, userFound.password);

  if (!matchpassword) {
    return res.status(401).json({ toke: null, message: "[Error] something goes wrong" });
  }

  let fullName =userFound.fullName

  const token = jwt.sign(
    { id: userFound.id },
    process.env.ACCESS_TOKEN_PRIVATE_KEY,
    {
      expiresIn: 86400, //24 hrs
    }
  );

  res.status(201).json({ token, fullName,message: "Logged in successfully!" });
};

export const restorePassword = async (req, res) => {
  const { email, password } = req.body;
  const userFound = await getUserByEmail(email);
  console.log(userFound);

  if (!userFound) {
    return res.status(400).json({ message: "Error something goes wrong" });
  }

  const newpassword = resetPassword(password, userFound.id);

  res.status(201).json({ message: "resetPassword in successfully!" });
};
