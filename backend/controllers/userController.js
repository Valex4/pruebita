import bcycript from "bcryptjs";
import { query } from "../connection.js";

export const encryptPassword = async (password) => {
  const salt = await bcycript.genSalt(10);
  return await bcycript.hash(password, salt);
};

export const getUsers = async (req, res) => {
  try {
    const [result] = await query("SELECT * FROM Users", []);
    res.status(201).json(result);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getUserByEmail = async (email) => {
  try {
    const [result] = await query("SELECT * FROM Users WHERE email= ?", [email]);
    if (result.length === 0) {
      return "[Error] something goes wrong"
    }
    console.log(result[0]);
    return result[0];
  } catch (error) {
    console.log("Error something goes wrong" + error);
  }
};

export const createUser = async (
  fullName,
  gender,
  birthDate,
  email,
  password
) => {
  try {
    let passwordEncript = await encryptPassword(password);
    const sql =
      "INSERT INTO Users (fullName,gender,birthDate,email,password) VALUES (?,?,?,?,?)";
    const params = [fullName, gender, birthDate, email, passwordEncript];
    const [result] = await query(sql, params);
  } catch (error) {
    return error;
  }
};

export const updateUser = async (req, res) => {
  try {
    const [result] = await query("UPDATE Users SET ? WHERE id=?", [
      req.body,
      req.params.id,
    ]);

    res.status(201).json(result);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const deleteUser = async (req, res) => {
  try {
    const [result] = await query("DELETE FROM Users WHERE id = ?", [
      req.params.id,
    ]);

    if (result.affectedRows === 0) {
      return res.status(404).json({ message: "Error something goes wrong" });
    }
    return res.sendStatus(204);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const comparePassword = async (password, receivedPassword) => {
  return await bcycript.compare(password, receivedPassword);
};

export const resetPassword = async (password, idUser) => {
  try {
    let newpasswordEncript = await encryptPassword(password);
    const [result] = await query("UPDATE Users SET password=? WHERE id=?", [
      newpasswordEncript,
      idUser,
    ]);
    return result
  } catch (error) {
    return error
  }
};
