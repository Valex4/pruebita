import { query } from "../connection.js";
import { deleteFile } from "./fileController.js";

export const getAll = async (req, res) => {
  try {
    const [result] = await query("SELECT * FROM Cards", []);
    res.status(201).json(result);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getCard = async (req, res) => {
  try {
    const [result] = await query("SELECT * FROM Cards WHERE name = ?", [
      req.params.name,
    ]);
    if (result.length === 0) {
      return res.status(404).json({ message: "Card not found" });
    }
    res.json(result[0]);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const newCard = async (req, res) => {
  try {
    const { name, canvas_data, images, background, preview } = req.body;

    const sql =
      "INSERT INTO Cards (name, canvas_data, images, background, preview) VALUES (?, ?, ?, ?, ?)";
    const params = [name, canvas_data, images, background, preview];
    const [result] = await query(sql, params);
    res.status(201).json({ message: "Card created successfully!" });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const updateCard = async (req, res) => {
  try {
    const { id, canvas_data, images, background, preview } = req.body;
    const sql =
      "UPDATE Cards SET canvas_data = ?, images = ?, background = ?, preview = ? WHERE id = ?";
    const [result] = await query(sql, [
      canvas_data,
      images,
      background,
      preview,
      id,
    ]);
    console.log(result);

    res.status(201).json({ message: "Card updated successfully!" });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const deleteCard = async (req, res) => {
  const [response] = await query("Select images, preview, background from Cards where id = ?",[req.params.id]);
  console.log("Imprimiendo las imagenes, preview, background");
  console.log(response[0]);

  const imagenesJSON = response[0].images;
  let nombresImagenesAEliminar = [];

  const datosRecibidos = {
    background: response[0].background,
    preview: response[0].preview
  };

  try {
      const imagenesArreglo = JSON.parse(imagenesJSON);
      nombresImagenesAEliminar = imagenesArreglo.filter(nombre => typeof nombre === 'string' && !nombre.startsWith('#'));
    } catch (error) {
      console.error("Error al analizar el JSON de imágenes:", error);
    }

    // Agregar el resto de los nombres de imágenes a la lista
    nombresImagenesAEliminar = nombresImagenesAEliminar.concat(Object.values(datosRecibidos).filter(nombre => typeof nombre === 'string' && !nombre.startsWith('#')));

    console.log("Imprimiendo el arreglo de las imagenes: ");
    console.log(nombresImagenesAEliminar);

  try {
    const resultado = deleteFile(nombresImagenesAEliminar);
    const [result] = await query("DELETE FROM Cards WHERE id = ?", [
      req.params.id,
    ]);

    if (result.affectedRows === 0) {
      return res.status(404).json({ message: "Error something goes wrong" });
    }

    res.status(201).json({ message: "Card deleted succesfully!" });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};
