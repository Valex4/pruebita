import multer from "multer";
import path from "path";
import  fs  from "fs";

const imgconfig = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./uploads");
  },
  filename: function (req, file, cb) {
    console.log("FILE: " + file.originalname);
    cb(null, `${file.originalname}`);
  },
});

const isImage = (req, file, callback) => {
  if (file.mimetype.startsWith("image")) {
    callback(null, true);
  } else {
    callback(null, Error("only image is allowd"));
  }
};

export let upload = multer({
  storage: imgconfig,
  fileFilter: isImage,
}).single("image");

export const createFile = async (req, res) => {
  try {
    const { filename } = req.file;
    console.log("imprimiendo filename: " + filename);
    res.status(201).json({ message: "save succesfuly.." });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: error.message });
  }
};

export const deleteFile = (nombresImagenes) => {
  console.log(import.meta.url);
  const moduleDirname = path.dirname(new URL(import.meta.url).pathname);
  const carpeta = path.join(moduleDirname, '..','uploads');
  console.log(carpeta)
  nombresImagenes.forEach(nombreImagen => {
    const rutaImagen = path.join(carpeta, nombreImagen);

    if (fs.existsSync(rutaImagen)) {
      fs.unlinkSync(rutaImagen);
      console.log(`Imagen ${nombreImagen} eliminada`);
    } else {
      console.log(`La imagen ${nombreImagen} no existe`);
    }
  });
}
