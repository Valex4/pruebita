import { query } from "../connection.js";

export const getAll = async (req, res) => {
  try {
    const [result] = await query("SELECT * FROM BirthdayBoy", []);
    res.status(201).json(result);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getBirthdayBoy = async (req, res) => {
  try {
    const [result] = await query("SELECT * FROM BirthdayBoy WHERE id = ?", [
      req.params.id,
    ]);
    if (result.length === 0) {
      return res.status(404).json({ message: "BirthdayBoy not found" });
    }
    res.json(result[0]);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const newBirthdayBoy = async (req, res) => {
  try {
    const sql = "INSERT INTO BirthdayBoy (fullName,birthDate) VALUES (?,?)";
    const params = [req.body.fullName, req.body.birthDate];
    const [result] = await query(sql, params);
    res.status(201).json({ message: "BirthdayBoy created succesfuly!" });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const updateBirthdayBoy = async (req, res) => {
  try {
    const { fullName, birthDate } = req.body;

    const sql = "UPDATE BirthdayBoy SET fullName=?, birthDate=? WHERE id=?";

    const [result] = await query(sql, [fullName, birthDate, req.params.id]);
    console.log(result);

    res.status(201).json({ message: "BirthdayBoy update succesfuly!" });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const deleteBirthdayBoy = async (req, res) => {
  try {
    const [result] = await query("DELETE FROM BirthdayBoy WHERE id = ?", [
      req.params.id,
    ]);

    if (result.affectedRows === 0) {
      return res.status(404).json({ message: "Error something goes wrong" });
    }

    return res.sendStatus(204);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getNext = async (req, res) => {
  try {
    const [result] = await query("SELECT * FROM BirthdayBoy", []);

    const currentDate = new Date(); //la fecha actual
    const currentMonth = currentDate.getMonth() + 1; //accedemos al mes actual 
    const currentDay = currentDate.getDate(); //accedemos al dia actual 

    //SORT es un metodo de ordenamiento 
    
    result.sort((a, b) => {
      const dateA = new Date(a.birthDate);
      const dateB = new Date(b.birthDate);
    
      if (dateA.getMonth() === currentMonth - 1 && dateA.getDate() === currentDay) {
        return -1;
      }
    
      if (dateB.getMonth() === currentMonth - 1 && dateB.getDate() === currentDay) {
        return 1;
      }
    
      const diffA = Math.abs(dateA.getMonth() - currentMonth) * 31 + Math.abs(dateA.getDate() - currentDay);
      const diffB = Math.abs(dateB.getMonth() - currentMonth) * 31 + Math.abs(dateB.getDate() - currentDay);
    
      return diffA - diffB;
    });
    
    res.status(201).json(result);
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};
