import { getUserByEmail } from "../controllers/userController.js";
export const checkDuplicateUsernameOrEmail = async (req, res, next) => {
  /* este middleware evalua que no se registre otra cuenta con el mismo correo */
  const userFind = await getUserByEmail(req.body.email);
  if (userFind != undefined) {
    let email = JSON.stringify(userFind.email);
    if (email) {
      return res.status(400).json({ message: "[Error] email already in use" });
    }
  }
  next();
};