import { Router } from "express";
import { signIn, signUp,restorePassword } from "../controllers/authController.js";
import { checkDuplicateUsernameOrEmail } from "../middleware/verifysignup.js";
const routerAuth = Router();


routerAuth.post("/signin", signIn); //login
routerAuth.post("/signup",[checkDuplicateUsernameOrEmail] ,signUp); //registro
routerAuth.post("/restore", restorePassword)


export default routerAuth;