import { Router } from "express";
import { getCard,getAll,newCard,updateCard,deleteCard  } from "../controllers/cardController.js";
const routerCard = Router();

routerCard.get("/cards/getAll", getAll);
routerCard.get("/cards/:name", getCard);
routerCard.post("/cards/create", newCard);
routerCard.put("/cards/:id", updateCard);
routerCard.delete("/cards/:id", deleteCard);



export default routerCard;