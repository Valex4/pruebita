import { Router } from "express";
import { newBirthdayBoy,getAll,deleteBirthdayBoy,updateBirthdayBoy,getBirthdayBoy, getNext } from "../controllers/birthdayBoyController.js";
const routerBirthdayBoy = Router();

routerBirthdayBoy.get("/birthdayBoy/getAll",getAll)
routerBirthdayBoy.post("/birthdayBoy/create",newBirthdayBoy)
routerBirthdayBoy.put("/birthdayBoy/:id",updateBirthdayBoy)
routerBirthdayBoy.get("/birthdayBoy/:id",getBirthdayBoy)
routerBirthdayBoy.delete("/birthdayBoy/:id",deleteBirthdayBoy)
routerBirthdayBoy.get("/next", getNext)


export default routerBirthdayBoy;