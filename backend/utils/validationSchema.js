import Joi from "joi";
//import passwordComplexity from "joi-password-complexity";

export const signUpBodyValidation=(body)=>{
    //validando el req.body
    const schema=Joi.object({
        fullName:Joi.string().required().messages({
            'string.empty' : "Ingresa tu nombre"
        }),
        email:Joi.string().email().required().messages({
            "string.email": "Ingresa un correo valido",
            'string.empty' : "Ingresa una direccion"
        }),
        password:Joi.string().required().messages({
            "string.empty": "Ingresa una contraseña"
        }),
        birthDate:Joi.date().required().messages({
            'any.required': "Ingresa una fecha de nacimiento",
            "date.base": "ingresa una fecha de nacimiento",
            
        }),
        gender: Joi.string().required().valid('masculino', 'femenino', 'otro').messages(
            {
                "any.required": "Selecciona una opción",
                "string.base": "Selecciona una opción válida",
                "any.only": "Selecciona una opción válida",
            }
        )
    })
    return schema.validate(body)
}

export const loginBodyValidation=(body)=>{
    const schema=Joi.object({
        email:Joi.string().email().required().messages({
            "any.required": "Contraseña o Correo incorrecto",
            "string.email": "Ingresa un correo valido",
            'string.empty' : "Ingresa una direccion"
          }),
        password:Joi.string().required().messages({
            "any.required":"Contraseña o Correo incorrecto",
            "string.empty": "Ingresa una contraseña"
        })
    })
    return schema.validate(body)

}

